#Given a binary array nums, return the maximum number of consecutive 1's in the array.
def findMaxConsecutiveOnes(nums):
    maxLen = 0
    temp = 0
    for num in nums:
        if(num == 1):
            temp +=1
            if temp > maxLen:
                maxLen = temp
        else:
            if temp > maxLen:
                maxLen = temp
            temp = 0
    return maxLen

nums = [1,1,0,1,1,1]
print(findMaxConsecutiveOnes(nums))