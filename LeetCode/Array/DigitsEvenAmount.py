# Given an array nums of integers, return how many of them contain an even number of digits.
def findNumbers(nums):
    evenCount = 0
    for num in nums:
        if(len(str(num))%2 == 0):
            evenCount+=1
    return evenCount

nums = [12,345,2,6,7896]
print(findNumbers(nums))