# Given two strings, a and b, that may or may not be of the same length, determine the minimum number of character deletions required to make a and b anagrams.
# Any characters can be deleted from either of the strings.

def makeAnagram(a, b):
    count = 0
    for x in a:
        if(x not in b):
            count += 1
        else:
            b = b.replace(x, "", 1)
            a = a.replace(x, "", 1)
    for y in b:
        if(y not in a):
            count+= 1
        else:
            a = a.replace(y, "", 1)
            b = b.replace(y, "", 1)
    return count
print(makeAnagram("aabasdbasd", "aaa"))