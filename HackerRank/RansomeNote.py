#Harold is a kidnapper who wrote a ransom note, but now he is worried it will be traced back to him through his handwriting.
# He found a magazine and wants to know if he can cut out whole words from it and use them to create an untraceable replica of his ransom note.
# The words in his note are case-sensitive and he must use only whole words available in the magazine. He cannot use substrings or concatenation to create the words he needs.
# Given the words in the magazine and the words in the ransom note, print Yes if he can replicate his ransom note exactly using whole words from the magazine; otherwise, print No.

# Done with returns, "Hackerrank solution" below
def checkMagazine(magazine, note):
    magDic = {}
    for word in magazine:
        if word in magDic:
            magDic[word] +=1
        else:
            magDic[word]=1
    for n in note:
        if n in magDic:
            if magDic[n] >0:
                magDic[n] -=1
            else:
                return False
        else:
            return False
    return True


def checkMagazineHackerRank(magazine, note):
    magDic = {}
    for word in magazine:
        if word in magDic:
            magDic[word] +=1
        else:
            magDic[word]=1
    for n in note:
        if n in magDic:
            if magDic[n] >0:
                magDic[n] -=1
            else:
                print("No")
                return
        else:
            print("No")
            return
    print("Yes")
    return

print(checkMagazine(["aaa","bbb","ccc"],["ttt","bbb"]))