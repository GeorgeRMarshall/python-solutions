# Consider the following:
#
# A string, s, of length n where s = c0c1....cn-1.
# An integer, k, where k is a factor of n.
# We can split s into n/k substrings where each subtring, ti, consists of a contiguous block of k characters in s. Then, use each ti to create string ui such that:
#
# The characters in ui are a subsequence of the characters in ti.
# Any repeat occurrence of a character is removed from the string such that each character in ui occurs exactly once.
# In other words, if the character at some index j in ti occurs at a previous index <j in ti, then do not include the character in string ui.
# Given s and k, print n/k lines where each line i denotes string .

# Note this returns instead of just printing as asked in the question, it was altered for submission: see below
def merge_the_tools(string, k):
    stringList =[]
    while(len(string) >1):
        stringList.append("".join(sorted(set(string[:k]),key=string[:k].index)))
        string = string[k:]
    return(stringList)

print(merge_the_tools("AABCAAADA", 3))

def merge_the_tools_hackerrank(string, k):

    while(len(string) >0):
        print("".join(sorted(set(string[:k]),key=string[:k].index)))
        string = string[k:]

