# You are given an unordered array consisting of consecutive integers [1, 2, 3, ..., n] without any duplicates.
# You are allowed to swap any two elements.
# Find the minimum number of swaps required to sort the array in ascending order.
def minimumSwaps(arr):
    count = 0
    for y in range(len(arr)):
        for x in range(len(arr)-y):
            if(x+1 != arr[x]):
                temp = arr[x]
                arr[x] = arr[temp-1]
                arr[temp-1] = temp
                count+=1
    return count
test = [2, 31, 1, 38, 29, 5, 44, 6, 12, 18, 39, 9, 48, 49, 13, 11, 7, 27, 14, 33, 50, 21, 46, 23, 15, 26, 8, 47, 40, 3, 32, 22, 34, 42, 16, 41, 24, 10, 4, 28, 36, 30, 37, 35, 20, 17, 45, 43, 25, 19]
test1 = [6,3,4,2,5,1]
print(minimumSwaps(test1))